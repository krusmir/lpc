A monster survival game.

Pacing starts going faster and faster until you can't handle it anymore.

Fun and fast.

Compiled using IntelliJ Community Edition and lwjgl (tutorial for setting it up here)
http://www.lwjgl.org/wiki/index.php?title=Setting_Up_LWJGL_with_IntelliJ_IDEA