import org.newdawn.slick.*;

public class Game extends BasicGame {

    public BlockMap currentMap;
    public BlockMap transitionMap;
    public BlockMap worldMap[][];
    public int worldMapX;
    public int worldMapY;
    public int previous_worldMapX=-1;
    public int previous_worldMapY=-1;

    boolean gameStart,gameEnd,gamePaused;

    public GameMenu gameMenu;
    public GameMenu pausedMenu;
    public GameMenu gameEndMenu;

    long timestamp;

  //  public int charsAlive = 3;


    PC mainChar,William,Astraea;
/*    PC secondChar;
    PC thirdChar;*/

    PC deadChar;


    Boolean menuRequested = false;

    Dialog dialog;

    Music menuMusic;

    public Game() {
        super("Blob Mobs!");
        System.currentTimeMillis();
    }



    public void initWorldMap() throws SlickException
    {

        worldMap = new BlockMap[1][8];

        worldMap[0][0] = new BlockMap("data/B1.tmx");
        worldMap[0][1] = new BlockMap("data/B2.tmx");
        worldMap[0][2] = new BlockMap("data/B3.tmx");
        worldMap[0][3] = new BlockMap("data/B4.tmx");
        worldMap[0][4] = new BlockMap("data/B5.tmx");
        worldMap[0][5] = new BlockMap("data/B6.tmx");
        worldMap[0][6] = new BlockMap("data/B7.tmx");
        worldMap[0][7] = new BlockMap("data/B8.tmx");


        worldMapX = 0;
        worldMapY = 0;
        currentMap = worldMap[worldMapX][worldMapY];

    }

    public void initSound() throws SlickException{
        menuMusic = new Music("data/sounds/stoneFortress.ogg");
        menuMusic.loop();
    }

    public void init(GameContainer container) throws SlickException {
        container.setVSync(true);

        String ui = "lpc_images/ui/";

        gameStart = false;
        gameEnd = false;
        gamePaused = false;

        initSound();
        initMainMenus();
        initWorldMap();

   //     mainChar = new PC("lpc_images/mainCharacters/Astraea_walkcycle_FULL_cropped.png","lpc_images/mainCharacters/William_slash_cropped.png",9);
   //     mainChar.name = "Astrea";

        William = new PC("lpc_images/mainCharacters/William_walkcycle_cropped.png","lpc_images/mainCharacters/William_slash_cropped.png",9);
        William.name = "William";
        William.AddSpellAnimation("lpc_images/Miscellaneous/magic_firelion_sheet.png");

        Astraea = new PC("lpc_images/mainCharacters/Astraea_walkcycle_FULL_cropped.png","lpc_images/mainCharacters/Astraea_slash_cropped.png",9);
        Astraea.name = "Astraea";
        Astraea.AddSpellAnimation("lpc_images/Miscellaneous/magic_firelion_sheet.png");
        //secondChar = new PC("lpc_images/mainCharacters/BRivera-malesoldier_cropped.png","lpc_images/mainCharacters/William_slash_cropped.png",9) ;
        /*secondChar = new PC("lpc_images/mainCharacters/AgentWalk_cropped.png","lpc_images/mainCharacters/AgentShoot_cropped.png",9) ;
        secondChar.name = "Blue";
        thirdChar = new PC("lpc_images/mainCharacters/William_walkcycle_cropped.png","lpc_images/mainCharacters/William_slash_cropped.png",9);
        thirdChar.name = "William";
        thirdChar.AddSpellAnimation("lpc_images/Miscellaneous/magic_firelion_sheet.png");*/



   //     secondChar.following = mainChar;
   //     thirdChar.following = secondChar;




        dialog = new Dialog(ui+"cutscene_top.png",ui+"cutscene_bottom.png","Hello World", "Yeah");
        //her = new PC("data/femalebase.png");


        Astraea.map = this.currentMap;
        William.map = this.currentMap;
    /*    secondChar.map = this.currentMap;
        thirdChar.map = this.currentMap;

*/






        //NPC  enemy = new Blob_mob( 670, 660 );
/*        for(int x = 0; x < 25; x++){
            int xpos, ypos,animationSpeed;
            xpos =(int) ((Math.random() * 1000) +1124);
            ypos =(int)( Math.random() *768);
            animationSpeed = (int)(Math.random()*100) + 50 ;
            NPC enemy = new Blob_mob_aggresive(animationSpeed,xpos,ypos,100,4,0.2F);
           // NPC  enemy = new Blob_mob_aggresive((int)(Math.random()*100) + 50);
            enemy.name = "Blob";
           // System.out.println(enemy.animationSpeed);
            //enemy.animationSpeed = (int)(enemy.animationSpeed) * (int)(Math.random()*100);
            //System.out.println(enemy.animationSpeed);

            enemy.map = this.currentMap;
            this.currentMap.NPC.add(enemy);
           }*/

        this.currentMap.generateWaves(0,5,255,false);


    }

    void initMainMenus() throws SlickException {
        gameMenu = new GameMenu("lpc_images/ui/menu.png",1024,768);
        pausedMenu = new GameMenu("lpc_images/ui/menu_paper_1024x768.png",1024,768);

    }

    public void update(GameContainer container, int delta) throws SlickException {

        if(gameStart && !gamePaused)
        {
            int keyDown = -1;
            if (container.getInput().isKeyDown(Input.KEY_LEFT)) {
                    keyDown = Input.KEY_LEFT;
            }
            if (container.getInput().isKeyDown(Input.KEY_RIGHT)) {
                    keyDown = Input.KEY_RIGHT;
            }
            if (container.getInput().isKeyDown(Input.KEY_UP)) {
                    keyDown = Input.KEY_UP;
            }
            if (container.getInput().isKeyDown(Input.KEY_DOWN)) {
                    keyDown = Input.KEY_DOWN;
            }
            if (container.getInput().isKeyPressed(Input.KEY_C))
            {
                menuRequested=true;
            }
            if(container.getInput().isKeyDown(Input.KEY_SPACE))
            {
                keyDown = Input.KEY_SPACE;
            }
            if (container.getInput().isKeyPressed(Input.KEY_ESCAPE))
            {
                menuRequested=false;
            }
            if (container.getInput().isKeyPressed(Input.KEY_P))
            {
                this.gamePaused=true;
            }
        /*    if (container.getInput().isKeyPressed(Input.KEY_Z))
            {
                PC temp = null;
                //switch characters
                temp = mainChar;
                mainChar = secondChar;
                secondChar =temp;

                //switch follower
                thirdChar.following = secondChar;
                secondChar.following = mainChar;
                mainChar.following = null;

                this.currentMap.mainChar = mainChar;
            }
            if (container.getInput().isKeyPressed(Input.KEY_X))
            {
                PC temp = null;
                //switch characters
                temp = secondChar;
                secondChar = thirdChar;
                thirdChar =temp;

                //switch follower
                thirdChar.following = secondChar;
                secondChar.following = mainChar;
                mainChar.following = null;

                this.currentMap.mainChar = mainChar;
            }*/
    /*        if (container.getInput().isKeyPressed( Input.KEY_H ) ) {
                keyDown = Input.KEY_H;
            }
            if (container.getInput().isKeyPressed( Input.KEY_F ) ) {
                keyDown = Input.KEY_F;
            }
            if (container.getInput().isKeyPressed( Input.KEY_G ) ) {
                keyDown = Input.KEY_G;
            }*/

            mainChar.update(keyDown , delta );
    /*        secondChar.update(keyDown);
            thirdChar.update(keyDown);*/
            for(NPC enemy:currentMap.NPC)
            {
                enemy.update( keyDown );

            }
            this.currentMap.removeDeadNPC();
            // ADD NPC UPDATES (enemies and shit)
        }
        else
        {
            if(!gamePaused)
            {
            if (container.getInput().isKeyPressed(Input.KEY_1))
            {
               this.gameStart = true;
                mainChar = Astraea;

            }else if(container.getInput().isKeyPressed(Input.KEY_2) )
            {
                this.gameStart = true;
                mainChar = William;
            }
            this.currentMap.mainChar = mainChar;
            }
            else if (gamePaused)
            {
                if (container.getInput().isKeyPressed(Input.KEY_P))
                {
                    this.gamePaused = false;
                }

            }
        }

    }

    public void Animate_Transition(GameContainer container, Graphics g)
    {

        if( mainChar.leftTransition)
        {
            if(previous_worldMapX == -1 && previous_worldMapY == -1 )
            {
                transitionMap = worldMap[worldMapX][worldMapY];
                previous_worldMapX = worldMapX++;
                previous_worldMapY = worldMapY;
            }
            currentMap =  worldMap[worldMapX][worldMapY];

            for (int layer = BlockMap.GROUND_1; layer < BlockMap.BLOCK_SPACES; layer++)
            {
                currentMap.tmap.render((int)mainChar.playerX,0,layer);
                transitionMap.tmap.render((int)mainChar.playerX-1024,0,layer);
            }
      //      g.drawAnimation(secondChar.player,secondChar.playerX, secondChar.playerY);
            g.drawAnimation(mainChar.player,mainChar.playerX, mainChar.playerY);

        }
        if (mainChar.rightTransition)
        {
            if(previous_worldMapX == -1 && previous_worldMapY == -1 )
            {
                transitionMap = worldMap[worldMapX][worldMapY];
                previous_worldMapX = worldMapX--;
                previous_worldMapY = worldMapY;
            }
            currentMap =  worldMap[worldMapX][worldMapY];

            for (int layer = BlockMap.GROUND_1; layer < BlockMap.BLOCK_SPACES; layer++)
            {
                currentMap.tmap.render((int)mainChar.playerX-1024,0,layer);
                transitionMap.tmap.render((int)mainChar.playerX,0,layer);
            }
        //    g.drawAnimation(secondChar.player,secondChar.playerX, secondChar.playerY);
            g.drawAnimation(mainChar.player,mainChar.playerX, mainChar.playerY);


        }
        if( mainChar.upTransition)
        {
            if(previous_worldMapX == -1 && previous_worldMapY == -1 )
            {
                transitionMap = worldMap[worldMapX][worldMapY];
                previous_worldMapX = worldMapX;
                previous_worldMapY = worldMapY++;
            }
            currentMap =  worldMap[worldMapX][worldMapY];

            for (int layer = BlockMap.GROUND_1; layer < BlockMap.BLOCK_SPACES; layer++)
            {
                currentMap.tmap.render(0,(int)mainChar.playerY,layer);
                transitionMap.tmap.render(0,(int)mainChar.playerY-768,layer);
                // newMap.tmap.render(1024-mainChar.playerX,0,layer);
            }
      //      g.drawAnimation(secondChar.player,secondChar.playerX, secondChar.playerY);
            g.drawAnimation(mainChar.player,mainChar.playerX, mainChar.playerY);

        }
        if (mainChar.downTransition)
        {
            if(previous_worldMapX == -1 && previous_worldMapY == -1 )
            {
                transitionMap = worldMap[worldMapX][worldMapY];
                previous_worldMapX = worldMapX;
                previous_worldMapY = worldMapY--;
            }
            currentMap =  worldMap[worldMapX][worldMapY];

            for (int layer = BlockMap.GROUND_1; layer < BlockMap.BLOCK_SPACES; layer++)
            {
                currentMap.tmap.render(0,(int)mainChar.playerY-768,layer);
                transitionMap.tmap.render(0,(int)mainChar.playerY,layer);
            }
     //      g.drawAnimation(secondChar.player,secondChar.playerX, secondChar.playerY);
            g.drawAnimation(mainChar.player,mainChar.playerX, mainChar.playerY);


        }
       // currentMap = transitionMap;
        mainChar.map = currentMap;
     //   secondChar.map = currentMap;

    }

    public void render(GameContainer container, Graphics g) throws SlickException {
        if(gameStart && !gamePaused)
        {

            //Draw MAP
            if(mainChar.transitionFinished)
            {
                mainChar.transitionFinished = false;
                previous_worldMapX = -1;
                previous_worldMapY = -1;
            }
            // animate transition if doing that
            if(mainChar.leftTransition || mainChar.rightTransition ||
               mainChar.upTransition || mainChar.downTransition)
            {
                Animate_Transition(container,g);
                return;
            }

            // Animate ground and some structures (below character level)
            for (int x = BlockMap.GROUND_1; x < BlockMap.STRUCTURES_3;x++)
            {
                currentMap.tmap.render(0,0,x);
            }


            if( mainChar.health <= 0)
            {                     this.gamePaused = true;

                  //  System.exit(0);  //game over

            }

            // Animate characters
    /*        if (thirdChar != null)
            {
                g.drawAnimation(thirdChar.player,thirdChar.playerX, thirdChar.playerY);
            }
            if (secondChar != null)
            {
                g.drawAnimation(secondChar.player,secondChar.playerX, secondChar.playerY);
            }
            if (thirdChar != null)
            {*/
                g.drawAnimation(mainChar.player,mainChar.playerX, mainChar.playerY);
     //       }
            //System.out.println(mainChar.name+mainChar.playerX+mainChar.playerY);
            if(currentMap.NPC != null && currentMap.NPC.isEmpty() && currentMap.waves != null && !currentMap.waves.isEmpty())
            {
                currentMap.getNextWave();
            }
            for(NPC enemy:currentMap.NPC)
            {
                 g.drawAnimation(enemy.player,enemy.playerX,enemy.playerY);
                 //g.draw(enemy.playerPoly);
                //System.out.println(enemy.playerX + " " + enemy.playerY);
            }

            if (mainChar.attackAnimation != null)
            {   if(mainChar.lastKeyValue == Input.KEY_RIGHT)
                {
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX +32, mainChar.playerY,Color.orange);
                   // g.drawAnimation(mainChar.attackAnimation,mainChar.playerX +64, mainChar.playerY,Color.orange);
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX +96, mainChar.playerY,Color.yellow);
                   // g.drawAnimation(mainChar.attackAnimation,mainChar.playerX +128, mainChar.playerY);
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX +160, mainChar.playerY,Color.red);
                }
                if(mainChar.lastKeyValue == Input.KEY_LEFT)
                {
                   // int xmod = 96;
                  //  int ymod = -32;
                   // g.rotate(mainChar.playerX-xmod,mainChar.playerY-ymod,180);

                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX -64, mainChar.playerY,Color.orange);
                   // g.drawAnimation(mainChar.attackAnimation,mainChar.playerX -96, mainChar.playerY,Color.orange);
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX -128, mainChar.playerY,Color.yellow);
                   // g.drawAnimation(mainChar.attackAnimation,mainChar.playerX -160, mainChar.playerY);
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX -192, mainChar.playerY,Color.red);

                   // g.rotate(mainChar.playerX-xmod,mainChar.playerY-ymod,180);

                }
                if(mainChar.lastKeyValue == Input.KEY_UP)
                {
                    // int xmod = 96;
                    //  int ymod = -32;
                    // g.rotate(mainChar.playerX-xmod,mainChar.playerY-ymod,180);

                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX , mainChar.playerY-64,Color.orange);
                    // g.drawAnimation(mainChar.attackAnimation,mainChar.playerX -96, mainChar.playerY,Color.orange);
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX , mainChar.playerY -128,Color.yellow);
                    // g.drawAnimation(mainChar.attackAnimation,mainChar.playerX -160, mainChar.playerY);
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX , mainChar.playerY-192,Color.red);

                    // g.rotate(mainChar.playerX-xmod,mainChar.playerY-ymod,180);

                }
                if(mainChar.lastKeyValue == Input.KEY_DOWN)
                {
                    // int xmod = 96;
                    //  int ymod = -32;
                    // g.rotate(mainChar.playerX-xmod,mainChar.playerY-ymod,180);

                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX , mainChar.playerY+64,Color.orange);
                    // g.drawAnimation(mainChar.attackAnimation,mainChar.playerX , mainChar.playerY,Color.orange);
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX , mainChar.playerY+128,Color.yellow);
                    // g.drawAnimation(mainChar.attackAnimation,mainChar.playerX -160, mainChar.playerY);
                    g.drawAnimation(mainChar.attackAnimation,mainChar.playerX , mainChar.playerY + 192,Color.red);

                    // g.rotate(mainChar.playerX-xmod,mainChar.playerY-ymod,180);

                }


                mainChar.attackAnimation = null;
            }


            //g.draw(thirdChar.playerPoly);
            //g.draw(mainChar.playerPoly);
            //g.draw(secondChar.playerPoly);
            //g.draw(enemy.playerPoly);

            // Animate objects above character level
            for (int x =BlockMap.STRUCTURES_3; x < BlockMap.BLOCK_SPACES; x++)
            {
                currentMap.tmap.render(0,0,x);
            }

            // show menu if requested
            if(menuRequested)
            {
                g.drawImage(dialog.upperScreen,0,0);
                g.drawImage(dialog.lowerScreen,0,605);
            }

            // Show health bar
            g.drawAnimation(mainChar.healthBar.current,0,553);
        }
        else
        {
            if(!gamePaused)
            {
                gameMenu.Show(container,g);
            }
            else if (gamePaused)
            {
                pausedMenu.ShowScore(container,g,mainChar.score,mainChar.health);
            }

        }

    }

    public static void main(String[] argv) throws SlickException {
        AppGameContainer container =
                new AppGameContainer(new Game(),1024 , 768, false);
        container.start();
    }
}