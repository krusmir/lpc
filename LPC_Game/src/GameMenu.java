import org.newdawn.slick.*;
import org.newdawn.slick.font.effects.ColorEffect;


public class GameMenu
{

   // TrueTypeFont font;
    public String text1;
    public String text2;
    public String text3;
    public String text4;
    public String text5;

    UnicodeFont menuFont;
    SpriteSheet menu;
    Animation menuAnimation;

    public GameMenu(String mainMenuImage,int width, int height ) throws SlickException {

        menuAnimation = new Animation();
        try {
            menu = new SpriteSheet(mainMenuImage,width,height);

        } catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        menuAnimation.addFrame(menu.getSprite(0,0),1 );






    }

    public void Show(GameContainer container, Graphics g) throws SlickException {
        g.drawAnimation(menuAnimation,0,0);

        menuFont = new UnicodeFont("lpc_images/ui/Designer-Notes-Bold.ttf", 56 , false, false);
        menuFont.addAsciiGlyphs();
        menuFont.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
        menuFont.loadGlyphs();
        String msg =  "Press 1 for Astraea\nPress 2 for William";
        float x = container.getWidth()/2 - menuFont.getWidth(msg)/2;
        float y = 450;
        float o = 3;

        menuFont.drawString(x - o  , y - o , msg , Color.black );
        menuFont.drawString( x  , y , msg , Color.white );

    }

    public void ShowScore(GameContainer container, Graphics g,int score,float health) throws SlickException {
        g.drawAnimation(menuAnimation,0,0);

        if(health > 0)
        {

                menuFont = new UnicodeFont("lpc_images/ui/Designer-Notes-Bold.ttf", 56 , false, false);
                menuFont.addAsciiGlyphs();
                menuFont.getEffects().add(new ColorEffect(java.awt.Color.BLACK));
                menuFont.loadGlyphs();
                String msg =  "Score so far: "+score;
                float x = container.getWidth()/2 - menuFont.getWidth(msg)/2;
                float y = 450;
                float o = 3;

                menuFont.drawString(x - o  , y - o , msg , Color.black );
                menuFont.drawString( x  , y , msg , Color.white );
        }
        else
        {
            menuFont = new UnicodeFont("lpc_images/ui/Designer-Notes-Bold.ttf", 56 , false, false);
            menuFont.addAsciiGlyphs();
            menuFont.getEffects().add(new ColorEffect(java.awt.Color.BLACK));
            menuFont.loadGlyphs();
            String msg =  "FINAL SCORE: "+score;
            float x = container.getWidth()/2 - menuFont.getWidth(msg)/2;
            float y = 450;
            float o = 3;

            menuFont.drawString(x - o  , y - o , msg , Color.black );
            menuFont.drawString( x  , y , msg , Color.white );

        }

    }
}
