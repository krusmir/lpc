import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.TrueTypeFont;

public class Dialog
{

    SpriteSheet lowerScreen;
    SpriteSheet upperScreen;
    String upperText;
    String lowerText;

    TrueTypeFont font;


    public Dialog(String upperScreenImage,String lowerScreenImage,String upperText, String lowerText)
    {

        this.upperText = upperText;
        this.lowerText = lowerText;

        try {
            upperScreen = new SpriteSheet(upperScreenImage,1024,768);
            lowerScreen = new SpriteSheet(lowerScreenImage,1024,768);
        } catch (SlickException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
}