import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class HealthBar
{
    int animationSpeed;
    Animation full, green,greenDamaged,yellow,yellowDamaged,red,redDamaged,empty,current;
    public HealthBar(String spriteSheet)
    {

        animationSpeed = 300;
        SpriteSheet sheet = null;
        try {
            sheet = new SpriteSheet(spriteSheet,227,215);
        } catch (SlickException e) {
            e.printStackTrace();
        }

        empty = new Animation();
        for (int frame=0;frame<1;frame++) {
            empty.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }

        redDamaged = new Animation();
        for (int frame=1;frame<3; frame++) {
            redDamaged.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }

        red = new Animation();
        for (int frame=2;frame<3; frame++) {
            red.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }

        yellowDamaged = new Animation();
        for (int frame=3;frame<5;frame++) {
            yellowDamaged.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }

        yellow = new Animation();
        for (int frame=4;frame<5; frame++) {
            yellow.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }

        greenDamaged = new Animation();
        for (int frame=5;frame<7;frame++) {
            greenDamaged.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }

        green = new Animation();
        for (int frame=6;frame<7;frame++) {
            green.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }

        full = new Animation();
        for (int frame=0;frame<7; frame++) {
            full.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }

        current = full;

        //player.setAutoUpdate(true);
    }
}
