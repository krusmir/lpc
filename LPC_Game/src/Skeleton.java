import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Polygon;

/**
 * Created with IntelliJ IDEA.
 * User: Jp
 * Date: 8/1/12
 * Time: 2:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class Skeleton extends Blob_mob {

    Skeleton(){}

    Skeleton( int animationSpeed, int xcoor,int ycoor, int health, int mobSpeed, float attackDamage ){
        //super( animationSpeed, xcoor, ycoor, health, mobSpeed, attackDamage );
        super();

        this.centerX = xcoor;
        this.centerY = ycoor;

        this.playerX = centerX;
        this.playerY = centerY;

        this.attackDamage = attackDamage;
        this.health = health;
        this.maxHealth = health;
        this.playerSpeed = mobSpeed*(float)Math.random()  + 1;
        this.direction = rand.nextInt() % 5;

        this.animationSpeed = animationSpeed;
        setAnimation( 9, 32, 64);

        setCharacterSheet();

        setPolygon();

        this.player = still;
    }

    void setCharacterSheet() {

        // Character sheet
        alive = true;
        attackRadius = 2 * 32;
        range = 3 * 32;
        resistPhysical = 0;
        resistFire = 0;
        maxHealth = 30;
        health = maxHealth;

        playerX = centerX;
        playerY = centerY;


    }

    void setPolygon(){
        playerPoly = new Polygon(new float[]{
                playerX,playerY+32,
                playerX+32,playerY+32,
                playerX+32,playerY+64,
                playerX,playerY+64
        });
    }

    void setAnimation( int rows, int sizeX, int sizeY ){

        String spriteSheet = "lpc_images/enemies/BODY_skeleton_cropped.png";

        SpriteSheet sheet = null;
        try {
            sheet = new SpriteSheet(spriteSheet,sizeX,sizeY);
        } catch (SlickException e) {
            e.printStackTrace();
        }
        //Still Frames with Orientation and non
        still = new Animation();
        for (int frame=0;frame<1;frame++) {
            still.addFrame(sheet.getSprite(frame,2), animationSpeed*2);
        }
        downStill = new Animation();
        for (int frame=0;frame<1;frame= frame + 2) {
            downStill.addFrame(sheet.getSprite(frame, 2), animationSpeed);
        }
        rightStill = new Animation();
        for (int frame=0;frame<1;frame= frame + 2) {
            rightStill.addFrame(sheet.getSprite(frame,3), animationSpeed);
        }
        upStill = new Animation();
        for (int frame=0;frame<1;frame= frame + 2) {
            upStill.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }
        leftStill = new Animation();
        for (int frame=0;frame<1;frame= frame + 2) {
            leftStill.addFrame(sheet.getSprite(frame,1), animationSpeed);
        }

        //animations for directions
        down = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            down.addFrame(sheet.getSprite(frame,2), animationSpeed);
        }
        right = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            right.addFrame(sheet.getSprite(frame,3), animationSpeed);
        }
        up = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            up.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }
        left = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            left.addFrame(sheet.getSprite(frame,1), animationSpeed);
        }

    }

    void onHit( int directionOfDamage, int amountOfDamage, int typeOfDamage ){

        System.out.println( name + " is hit by " +amountOfDamage + " points of damage! "+ name +"'s health before damage calculation is: " + health);
        health -= damageReceived(amountOfDamage, typeOfDamage);
        if ( maxHealth < health ){
            health = maxHealth;
        }
        double lifePercentage = ( double ) health/ ( double ) maxHealth;

        System.out.println( name +"'s health after damage calculation is: " + health);
        System.out.println( name +" is at " + ( lifePercentage*100 ) + "% " + "("+lifePercentage+") life!");

        if ( lifePercentage <=0 ){
            health =0;
            alive = false;
        }

        System.out.println( name +"'s life status is: " + (alive?"alive":"dead"));

        //System.out.println(name +" hit in Center" + " for " + damageReceived( 3, PHYSICAL_DAMAGE)+ " by Physical Damage. " + "HP: "+ health + "/"+ maxHealth+ ". Status: alive = " + alive );
    }

    void update( int keyDownValue ) throws SlickException{
        attack_main_character();
        //System.out.println( name + " posx: " + playerX + " posy: " + playerY ) ;
    }
}
