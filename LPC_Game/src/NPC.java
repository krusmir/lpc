import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Polygon;

import java.util.Random;


/**
 * Created with IntelliJ IDEA.
 * User: Jp
 * Date: 7/26/12
 * Time: 12:08 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * TODO:
 *  - Fix the behaviour of the NPC in update() so the if the direction in CENTER it will only "move" half the distance.
 *  - Build stupid A.I.
 *  - Normalize the CENTER, UP, RIGHT, DOWN, LEFT values to be the same as their Input.Key_Press counterparts.
 */

public class NPC extends PC {

    public static final int CENTER = 0;
    public static final int UP = 1;
    public static final int RIGHT = 2;
    public static final int DOWN = 3;
    public static final int LEFT = 4;

    public static final int FULL_LIFE = 0;
    public static final int HALF_LIFE = 1;
    public static final int LOW_LIFE = 2;
    public static final int DEAD = 4;

    public static final int HEALING = 1300;
    public static final int PHYSICAL_DAMAGE = 1301;
    public static final int FIRE_DAMAGE = 1302;

    // Information needed to make the sprite animations
    String[] list_of_sprite_sheets;
    int rows_of_sprites;
    int size_of_sprite_in_X;
    int size_of_sprite_in_Y;

    int direction;
    int range;
    int distanceTravelled = 0;
    int maxHealth;
    Random rand = new Random();

    boolean alive = true;

    float centerX;
    float  centerY;
    boolean goBack = false;
    boolean collision = false;

    int attackRadius;

    // Resistances: range from -1 to 1, where values <0 represent a weakness to that type of damage,
    // and values >0 represent a strength to that type of damage.

    double resistPhysical = 0;
    double resistFire = 0;
    double resistHeal = 0;


    NPC() {
        super();  // Initializes the stacks and the histories
    }

    NPC ( String[] spriteSheets, int rows, int posX, int posY, int sizeX, int sizeY ) {
        /** The constructor for an NPC. This method takes as parameters: a list of relative directions
         * of the sprite sheets for the NPC as an array String, it uses the following positions:
         *      - 0: the sprite sheet for the full life animations
         *      - 1: the sprite sheet for the half life animations
         *      - 2: the sprite sheet for the low life animations ;
         * its starting position in x and y; the size
         * of the sprites, on x and y, measured in pixels; how much health the NPC has; and how close,
         * measured in number of blocks of 32x32, the PC can get to the NPC without evoking a response;
         * a measure in pixels of how much the NPC will move at a time; a measure in blocks of 32x32
         * of how far the NPC will move before returning to its starting point.
         */

        super();   // Initializes the stacks and the histories

        playerX = posX;
        playerY = posY;

        centerX = playerX;
        centerY = playerY;

        direction = rand.nextInt() % 5;

        playerSpeed = 4;
        animationSpeed = 100;

        list_of_sprite_sheets = spriteSheets;
        rows_of_sprites = rows;
        size_of_sprite_in_X = sizeX;
        size_of_sprite_in_Y = sizeY;

        setAnimation( list_of_sprite_sheets, FULL_LIFE, rows_of_sprites, size_of_sprite_in_X, size_of_sprite_in_Y );
        setPolygon();

        player = still;
    }

    void setAnimation(String[] spriteSheets, int whichAnimation, int rows, int sizeX, int sizeY ){

        String spriteSheet = spriteSheets[whichAnimation];

        SpriteSheet sheet = null;
        try {
            sheet = new SpriteSheet(spriteSheet,sizeX,sizeY);
        } catch (SlickException e) {
            e.printStackTrace();
        }
        still = new Animation();
        for (int frame=0;frame<rows;frame++) {
            still.addFrame(sheet.getSprite(frame,2), animationSpeed*2);
        }
        down = new Animation();
        for (int frame=0;frame<rows;frame++) {
            down.addFrame(sheet.getSprite(frame,2), animationSpeed);
        }
        left = new Animation();
        for (int frame=0;frame<rows;frame++) {
            left.addFrame(sheet.getSprite(frame,3), animationSpeed);
        }
        up = new Animation();
        for (int frame=0;frame<rows;frame++) {
            up.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }
        right = new Animation();
        for (int frame=0;frame<rows;frame++) {
            right.addFrame(sheet.getSprite(frame,1), animationSpeed);
        }

    }

    void setPolygon(){
        playerPoly = new Polygon(new float[]{
                playerX,playerY+32,
                playerX+32,playerY+32,
                playerX+32,playerY+64,
                playerX,playerY+64
        });
    }

    void returnToCenter(){
        /** This method switches the direction of the NPC towards
         * it's initial coordinate.
         */
        if  ( direction == RIGHT ){
            direction = LEFT;
        }
        else if ( direction == LEFT ){
            direction = RIGHT;
        }
        else if ( direction == UP ){
            direction = DOWN;
        }
        else if ( direction == DOWN ){
            direction = UP;
        }

    }

    boolean transition() {
        /** Removes the behaviour of the PC transition() method
         * so that the NPC does not "exit" from the map.
         */
        return false;
    }

    void move() throws SlickException{
        /** Given a direction, this method changes the x, y
         * coordinates of the NPC appropriately.
         */
        if ( direction == LEFT ) {
            //playerX--;
            playerX = playerX - playerSpeed;
            playerPoly.setX(playerX);
            if (entityCollisionWith()){
                playerX = playerX+playerSpeed;
                playerPoly.setX(playerX);
                collision = true;
            }

            player = left;
        }
        if ( direction == RIGHT ){

            playerX = playerX + playerSpeed;
            playerPoly.setX(playerX);
            if (entityCollisionWith()){
                playerX = playerX - playerSpeed;
                playerPoly.setX(playerX);
                collision = true;
            }
            player = right;
        }
        if ( direction == UP ) {

            // playerY--;
            playerY = playerY - playerSpeed;
            playerPoly.setY(playerY);
            if (entityCollisionWith()){
                playerY = playerY + playerSpeed;
                playerPoly.setY(playerY);
                collision = true;
            }
            player = up;
        }
        if ( direction == DOWN ) {
            //playerY++;
            playerY = playerY + playerSpeed;
            playerPoly.setY(playerY);
            if (entityCollisionWith()){
                playerY = playerY - playerSpeed;
                playerPoly.setY(playerY);
                collision = true;
            }
            player = down;
        }
        if ( direction == CENTER ) {
            player = still;
        }
    }

    void defaultMoveBehaviour() throws SlickException{
        if ( distanceTravelled == range || collision ){
            returnToCenter();
            goBack = true;
            collision = false;
        }
        move();

        if ( !goBack ){
            distanceTravelled += playerSpeed;
        }
        else if ( distanceTravelled == 0 ){
            direction = rand.nextInt() % 5;
            goBack = false;
        } else {
            distanceTravelled -= playerSpeed;
        }
    }

    void update( int keyDownValue ) throws SlickException{

        defaultMoveBehaviour();

    }

    int damageReceived(int amountOfDamage, int typeOfDamage){
        // Formula damageReceived = amount_of_type_damage * resistance_to_type_of_damage. Take into
        // consideration the strengths and weaknesses.

        int damage;
        damage = 0;

        if ( typeOfDamage == HEALING ){

            if ( resistHeal == 0 ){
                damage = -amountOfDamage;
             //   System.out.println(name + " has no ailment against healing! " + (amountOfDamage - damage) + " point(s) of healing absorbed!");
            }
            else if ( resistHeal < 0 ){
                damage = - ( ( int ) ( ( double ) amountOfDamage * ( 1.00 + Math.abs( resistHeal ) ) ) );
             //   System.out.println( name + " has a bonus to healing! " + ( damage - amountOfDamage ) + " point(s) of healing added!" );
            }
            else {
                damage =  - ( ( int ) ( ( double ) amountOfDamage * resistHeal ) );
             //   System.out.println(name + " has an ailment against healing! " + (amountOfDamage - damage) + " point(s) of healing absorbed!");
            }
        }
        if ( typeOfDamage == PHYSICAL_DAMAGE ){

            if ( resistPhysical == 0 ){
                damage = amountOfDamage;
               // System.out.println(name + " has no defence against physical damage! " + (amountOfDamage - damage) + " point(s) of damage absorbed!");
            }
            else if ( resistPhysical < 0 ){
                damage = ( int ) ( ( double ) amountOfDamage * ( 1.00 + Math.abs( resistPhysical ) ) );
             //   System.out.println( name + " is weak against physical damage! " + ( damage - amountOfDamage ) + " point(s) of damage added!" );
            }
            else {
                damage =  ( int ) ( ( double ) amountOfDamage * resistPhysical );
              //  System.out.println( name + " is resistant to physical damage! " + ( amountOfDamage - damage ) + " point(s) of damage absorbed!" );
            }
        }
        if ( typeOfDamage == FIRE_DAMAGE ){

            if ( resistFire == 0 ){
                damage = amountOfDamage;
             //   System.out.println( name + " has no defence against fire damage! " + ( amountOfDamage - damage ) + " point(s) of damage absorbed!" );
            }
            else if ( resistFire < 0 ){
                damage = ( int ) ( ( double ) amountOfDamage * ( 1.00 + Math.abs( resistFire ) ) );
             //   System.out.println( name + " is weak against fire damage! " + ( damage - amountOfDamage ) + " point(s) of damage added!" );
            }
            else {
                damage = ( int ) ( ( double ) amountOfDamage * resistFire );
              //  System.out.println( name + " is resistant to fire damage! " + (amountOfDamage - damage) + " point(s) of damage absorbed!" );
            }
        }

        return damage;
    }

    void onHit( int directionOfDamage, int amountOfDamage, int typeOfDamage ){

       System.out.println( name + " is hit by " +amountOfDamage + " points of damage! "+ name +"'s health before damage calculation is: " + health);
        health -= damageReceived(amountOfDamage, typeOfDamage);
        if ( maxHealth < health ){
            health = maxHealth;
        }
        double lifePercentage = ( double ) health/ ( double ) maxHealth;

        System.out.println( name +"'s health after damage calculation is: " + health);
        System.out.println( name +" is at " + ( lifePercentage*100 ) + "% " + "("+lifePercentage+") life!");

        if (  lifePercentage >  0 ){

            if ( ( lifePercentage <= ( double ) 1.0 && lifePercentage >= ( double ) .660) && typeOfDamage == HEALING){
                setAnimation( list_of_sprite_sheets, FULL_LIFE, rows_of_sprites, size_of_sprite_in_X, size_of_sprite_in_Y );
            } else
            if ( ( lifePercentage > ( double ) 0.66 ) && ( lifePercentage >= ( double ) 0.33 ) ){
                setAnimation( list_of_sprite_sheets, HALF_LIFE, rows_of_sprites, size_of_sprite_in_X, size_of_sprite_in_Y );
            } else
            if( ( lifePercentage > ( double ) 0.33 && lifePercentage >= ( double ) 0.01 ) ){
                setAnimation( list_of_sprite_sheets, LOW_LIFE, rows_of_sprites, size_of_sprite_in_X, size_of_sprite_in_Y );
            }
        } else
        if ( lifePercentage <=0 ){
            health =0;
            alive = false;
        }

      //  System.out.println( name +"'s life status is: " + (alive?"alive":"dead"));

        //System.out.println(name +" hit in Center" + " for " + damageReceived( 3, PHYSICAL_DAMAGE)+ " by Physical Damage. " + "HP: "+ health + "/"+ maxHealth+ ". Status: alive = " + alive );
    }

}
