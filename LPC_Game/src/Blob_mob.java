import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;

/**
 * Created with IntelliJ IDEA.
 * User: Jp
 * Date: 7/27/12
 * Time: 4:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class Blob_mob extends NPC {

    Blob_mob(){
        super(); // Initializes the stack
    }

    Blob_mob( int posX, int posY ){

        super(); // Initializes the stacks

        centerX = posX;
        centerY = posY;

        setCharacterSheet();

        direction = rand.nextInt() % 5;

        playerSpeed = 4;
        animationSpeed = 100;

        rows_of_sprites = 3;
        size_of_sprite_in_X = 32;
        size_of_sprite_in_Y = 32;
        list_of_sprite_sheets = new String[] {"lpc_images/enemies/slime.png", "lpc_images/enemies/yellow_slime.png", "lpc_images/enemies/magenta_slime.png"} ;

        setAnimation( list_of_sprite_sheets, FULL_LIFE, rows_of_sprites, size_of_sprite_in_X, size_of_sprite_in_Y );

        setPolygon();
        this.health = 100;
        this.maxHealth = 100;

        player = still;
    }


    Blob_mob( int posX, int posY,int animationSpeed ){

        super(); // Initializes the stacks

        centerX = posX;
        centerY = posY;

        setCharacterSheet();

        direction = rand.nextInt() % 5;

        playerSpeed = 4;
        this.animationSpeed = animationSpeed;

        rows_of_sprites = 3;
        size_of_sprite_in_X = 32;
        size_of_sprite_in_Y = 32;
        list_of_sprite_sheets = new String[] {"lpc_images/enemies/slime.png", "lpc_images/enemies/yellow_slime.png", "lpc_images/enemies/magenta_slime.png"} ;

        setAnimation( list_of_sprite_sheets, FULL_LIFE, rows_of_sprites, size_of_sprite_in_X, size_of_sprite_in_Y );

        setPolygon();
        this.health = 100;
        this.maxHealth = 100;

        player = still;
    }
    void setCharacterSheet() {

        // Character sheet
        alive = true;
        attackRadius = 2 * 32;
        range = 3 * 32;
        resistPhysical = 0.95;
        resistFire = -0.95;
        maxHealth = 30;
        health = maxHealth;

        playerX = centerX;
        playerY = centerY;


    }

    void setPolygon(){
        playerPoly = new Polygon(new float[]{
                playerX,playerY,
                playerX+32,playerY,
                playerX+32,playerY+32,
                playerX,playerY+32
        });
    }

    void update( int keyDownValue ) throws SlickException{

        defaultMoveBehaviour();

    }

    void checkCollision() throws SlickException
    {

        if (entityCollisionWith()){
            playerX = playerX+playerSpeed;
            playerPoly.setX(playerX);
        }

        if (entityCollisionWith()){
            playerX = playerX - playerSpeed;
            playerPoly.setX(playerX);
        }

        if (entityCollisionWith()){
            playerY = playerY + playerSpeed;
            playerPoly.setY(playerY);
        }

        if (entityCollisionWith()){
            playerY = playerY - playerSpeed;
            playerPoly.setY(playerY);
        }
    }

    void attack_main_character() throws SlickException
    {
        float xDistance = Math.abs(this.playerX - this.map.mainChar.playerX);
        float yDistance = Math.abs(this.playerY - this.map.mainChar.playerY);
        if (yDistance < xDistance)
        {
            player = this.playerX < this.map.mainChar.playerX ? this.right: this.left;
            this.playerX = this.playerX + (this.playerX < this.map.mainChar.playerX ?+this.playerSpeed/2:-this.playerSpeed/2);
            playerPoly.setX(playerX);
        }
        else
        {
            player = this.playerY < this.map.mainChar.playerY ? this.down:this.up;
            this.playerY = this.playerY + (this.playerY < this.map.mainChar.playerY ?+this.playerSpeed/2:-this.playerSpeed/2);
            playerPoly.setY(playerY);
        }

        checkCollision();

        if( this.playerX > map.mainChar.playerX      &&
                this.playerX < map.mainChar.playerX + 32 &&
                this.playerY < map.mainChar.playerY + 64 &&
                this.playerY >map.mainChar.playerY)
        {


            this.map.mainChar.receiveDamage(this.attackDamage);
        }

        if(this.health == 0)
        {
            this.playerSpeed = 0;
        }
    }
}
