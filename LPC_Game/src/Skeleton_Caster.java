import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Created with IntelliJ IDEA.
 * User: Jp
 * Date: 8/1/12
 * Time: 8:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class Skeleton_Caster extends Skeleton{

    Skeleton_Caster(){}

    Skeleton_Caster( int animationSpeed, int xcoor,int ycoor, int health, int mobSpeed, float attackDamage ){

        super( animationSpeed, xcoor, ycoor, health, mobSpeed, attackDamage );


    }

    void setCastAnimation( int rows, int sizeX, int sizeY ){

        String attackSheet = "lpc_images/enemies/BODY_skeleton_spell_cropped.png";

        SpriteSheet  sheet = null;
        try {
            sheet = new SpriteSheet(attackSheet,32,64);
        } catch (SlickException e) {
            e.printStackTrace();
        }

        attackDown = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            attackDown.addFrame(sheet.getSprite(frame,2), attackSpeed);
        }
        attackRight = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            attackRight.addFrame(sheet.getSprite(frame,3), attackSpeed);
        }
        attackUp = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            attackUp.addFrame(sheet.getSprite(frame,0), attackSpeed);
        }
        attackLeft = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            attackLeft.addFrame(sheet.getSprite(frame,1), attackSpeed);
        }

    }

    void attack_main_character() throws SlickException
    {
        float xDistance = Math.abs(this.playerX - this.map.mainChar.playerX);
        float yDistance = Math.abs(this.playerY - this.map.mainChar.playerY);
        if (yDistance < xDistance)
        {
            player = this.playerX < this.map.mainChar.playerX ? this.right: this.left;
            this.playerX = this.playerX + (this.playerX < this.map.mainChar.playerX ?+this.playerSpeed/2:-this.playerSpeed/2);
            playerPoly.setX(playerX);
        }
        else
        {
            player = this.playerY < this.map.mainChar.playerY ? this.down:this.up;
            this.playerY = this.playerY + (this.playerY < this.map.mainChar.playerY ?+this.playerSpeed/2:-this.playerSpeed/2);
            playerPoly.setY(playerY);
        }

        checkCollision();

        if( this.playerX > map.mainChar.playerX      &&
                this.playerX < map.mainChar.playerX + 32 &&
                this.playerY < map.mainChar.playerY + 64 &&
                this.playerY >map.mainChar.playerY)
        {


            this.map.mainChar.receiveDamage(this.attackDamage);
        }

        if(this.health == 0)
        {
            this.playerSpeed = 0;
        }
    }
}
