import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 * Created with IntelliJ IDEA.
 * User: krusmir
 * Date: 7/31/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class Blob_mob_aggresive extends Blob_mob {


    public Blob_mob_aggresive()
    {
        super((int)(10*Math.random()+1024),(int)(10*Math.random()+768));


    }

    public Blob_mob_aggresive(int animationSpeed)
    {
        super((int)(10*Math.random() +1024),(int)(10*Math.random()+768),animationSpeed);
    }

    public Blob_mob_aggresive(int animationSpeed,int xcoor, int ycoor, int health ,int mobSpeed, float attackDamage)
    {
        super(xcoor,ycoor,animationSpeed);
        this.attackDamage = attackDamage;
        this.health = health;
        this.playerSpeed =    mobSpeed*(float)Math.random()  + 1;

    }

    void update( int keyDownValue ) throws SlickException
    {

            attack_main_character();

        //System.out.println( name + " posx: " + playerX + " posy: " + playerY ) ;

    }


    void checkCollision() throws SlickException
    {

            if (entityCollisionWith()){
                playerX = playerX+playerSpeed;
                playerPoly.setX(playerX);
            }

            if (entityCollisionWith()){
                playerX = playerX - playerSpeed;
                playerPoly.setX(playerX);
            }

            if (entityCollisionWith()){
                playerY = playerY + playerSpeed;
                playerPoly.setY(playerY);
            }

            if (entityCollisionWith()){
                playerY = playerY - playerSpeed;
                playerPoly.setY(playerY);
            }
    }

    void attack_main_character() throws SlickException
    {
        float xDistance = Math.abs(this.playerX - this.map.mainChar.playerX);
        float yDistance = Math.abs(this.playerY - this.map.mainChar.playerY);
        if (yDistance < xDistance)
        {
            player = this.playerX < this.map.mainChar.playerX ? this.right: this.left;
            this.playerX = this.playerX + (this.playerX < this.map.mainChar.playerX ?+this.playerSpeed/2:-this.playerSpeed/2);
            playerPoly.setX(playerX);
        }
        else
        {
            player = this.playerY < this.map.mainChar.playerY ? this.down:this.up;
            this.playerY = this.playerY + (this.playerY < this.map.mainChar.playerY ?+this.playerSpeed/2:-this.playerSpeed/2);
            playerPoly.setY(playerY);
        }

        checkCollision();

        if( this.playerX > map.mainChar.playerX      &&
            this.playerX < map.mainChar.playerX + 32 &&
            this.playerY < map.mainChar.playerY + 64 &&
            this.playerY >map.mainChar.playerY)
        {


            this.map.mainChar.receiveDamage(this.attackDamage);
        }

        if(this.health == 0)
        {
            this.playerSpeed = 0;
        }
    }


}
