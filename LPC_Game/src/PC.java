import org.newdawn.slick.*;
import org.newdawn.slick.geom.Polygon;

import java.util.ArrayList;
import java.util.Stack;

public class PC
{

    public HealthBar healthBar;

    public float health;

    public String name;

    public int score = 0;

    public boolean  leftTransition = false;
    public boolean  rightTransition = false;
    public boolean upTransition = false;
    public boolean downTransition = false;

    public boolean transitionFinished=false;

    public float attackDamage ;

    public Animation player;
    public Animation down;
    public Animation right;
    public Animation up;
    public Animation left;
    public Animation still;
    public Animation die;

    public Animation downStill;
    public Animation rightStill;
    public Animation upStill;
    public Animation leftStill;

    public Animation attackDown;
    public Animation attackRight;
    public Animation attackUp;
    public Animation attackLeft;

    public Animation attackAnimation;
    public Animation spellAttackRight;
    public Animation spellAttackLeft;


    public float playerX=0;
    public float playerY=240;
    public Polygon playerPoly;
    public BlockMap map;


    public int lastKeyValue;

    public int animationSpeed;
    public int attackSpeed;
    public float playerSpeed;

    public Stack<Integer> actionHistory;
    public Stack<Float> positionXHistory;
    public Stack<Float> positionYHistory;

    public float positionX5Ago;
    public float positionY5Ago;
    public int followingDistance = 15;


    public PC following;

    Sound attackSound;

    PC()
    {
        positionX5Ago = -1;
        positionY5Ago = -1;
        actionHistory = new Stack<Integer>();
        positionXHistory = new Stack<Float>();
        positionYHistory = new Stack<Float>();

        playerSpeed = 4;
        animationSpeed = 40;
        attackSpeed = 160;
        health = 60;


    }

    PC(String spriteSheet, int rows)
    {
        this();

        String dieSheet =  "lpc_images/Miscellaneous/explotion_clean_320x128.png"      ;
        healthBar = new HealthBar("lpc_images/ui/lifebar_animation.png");

        SpriteSheet sheet = null;
        try {
            sheet = new SpriteSheet(spriteSheet,32,64);
        } catch (SlickException e) {
            e.printStackTrace();
        }
        //Still Frames with Orientation and non
        still = new Animation();
        for (int frame=0;frame<1;frame++) {
            still.addFrame(sheet.getSprite(frame,2), animationSpeed*2);
        }
        downStill = new Animation();
        for (int frame=0;frame<1;frame= frame + 2) {
            downStill.addFrame(sheet.getSprite(frame, 2), animationSpeed);
        }
        rightStill = new Animation();
        for (int frame=0;frame<1;frame= frame + 2) {
            rightStill.addFrame(sheet.getSprite(frame,3), animationSpeed);
        }
        upStill = new Animation();
        for (int frame=0;frame<1;frame= frame + 2) {
            upStill.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }
        leftStill = new Animation();
        for (int frame=0;frame<1;frame= frame + 2) {
            leftStill.addFrame(sheet.getSprite(frame,1), animationSpeed);
        }

        //animations for directions
        down = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            down.addFrame(sheet.getSprite(frame,2), animationSpeed);
        }
        right = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            right.addFrame(sheet.getSprite(frame,3), animationSpeed);
        }
        up = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            up.addFrame(sheet.getSprite(frame,0), animationSpeed);
        }
        left = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            left.addFrame(sheet.getSprite(frame,1), animationSpeed);
        }
        playerPoly = new Polygon(new float[]{
                playerX,playerY+32,
                playerX+32,playerY+32,
                playerX+32,playerY+64,
                playerX,playerY+64
        });
        player = still;
        //player.setAutoUpdate(true);


        addDieAnimation(dieSheet);

        //Initialize Sounds
        try{
         attackSound  = new Sound("data/sounds/attack.aif");
        }catch( SlickException e ){

        }

    }

    PC(String movementSheet, String attackSheet, int rows)
    {
        this(movementSheet,rows);

        SpriteSheet  sheet = null;
        try {
            sheet = new SpriteSheet(attackSheet,32,64);
        } catch (SlickException e) {
            e.printStackTrace();
        }

        attackDown = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            attackDown.addFrame(sheet.getSprite(frame,2), attackSpeed);
        }
        attackRight = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            attackRight.addFrame(sheet.getSprite(frame,3), attackSpeed);
        }
        attackUp = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            attackUp.addFrame(sheet.getSprite(frame,0), attackSpeed);
        }
        attackLeft = new Animation();
        for (int frame=0;frame<rows;frame= frame + 2) {
            attackLeft.addFrame(sheet.getSprite(frame,1), attackSpeed);
        }


    }

    public void AddSpellAnimation(String spellSheet)
    {

        SpriteSheet  sheet = null;
        try {
            sheet = new SpriteSheet(spellSheet,64,64);
        } catch (SlickException e) {
            e.printStackTrace();
        }

        spellAttackRight = new Animation();
        spellAttackLeft = new Animation();
        for (int frame=0;frame<4;frame= frame + 1) {
            for (int framey=0; framey < 4; framey = framey+ 1)
            {
                spellAttackRight.addFrame(sheet.getSprite(frame,framey), attackSpeed);
                spellAttackLeft.addFrame(sheet.getSprite(frame,framey).getFlippedCopy(true,false),attackSpeed);
            }


        }

        //sheet.getSprite(0,0).getFlippedCopy(true,true);

    }

    public void addDieAnimation(String dieSheet)
    {
        SpriteSheet  sheet = null;
        try {
            sheet = new SpriteSheet(dieSheet,64,64);
        } catch (SlickException e) {
            e.printStackTrace();
        }

        die = new Animation();

        for (int frame=0;frame<5;frame= frame + 1) {
            for (int framey=0; framey < 2; framey = framey+ 1)
            {
                die.addFrame(sheet.getSprite(frame,framey), attackSpeed);
            }
        }
    }


    public boolean entityCollisionWith() throws SlickException {
        for (int i = 0; i < this.map.entities.size(); i++) {
            Block entity1 = (Block) this.map.entities.get(i);
            if (playerPoly.intersects(entity1.poly)) {
                return true;
            }
        }
        return false;
    }

    boolean transition()
    {

        int margin = 16 ;
        boolean inTransition;
        if (inTransition = (playerX > 1024 - margin  ||  leftTransition  ||
                            playerX < 0 - margin     ||  rightTransition ||
                            playerY > 768 - margin*3   ||  upTransition    ||
                            playerY < 0 - margin*3     ||  downTransition  )
            )
        {
            if (playerX > 1024 - margin || leftTransition )
            {
                leftTransition = true;
                playerX = playerX - 32;
                if (playerX < 0)
                {
                    playerX = 0;
                    leftTransition = false;
                    transitionFinished = true;
                }
            }
            if (playerX < 0 - margin || rightTransition)
            {
                rightTransition = true;
                playerX = playerX + 32;
                if (playerX > 1024)
                {
                    playerX = 1024 - 32;
                    rightTransition = false;
                    transitionFinished = true;

                }
            }
            if (playerY > 768 - margin*3 || upTransition)
            {
                upTransition = true;
                playerY = playerY - 32;
                if (playerY < -margin*3)
                {
                    playerY = -margin*3;
                    upTransition = false;
                    transitionFinished = true;
                }
            }
            if (playerY < 0 - margin*3 || downTransition)
            {
                downTransition = true;
                playerY = playerY + 32;
                if (playerY > 768 - margin*3)
                {
                    playerY = 768 - margin*3;
                    downTransition = false;
                    transitionFinished = true;

                }
            }
        }

        return inTransition;
    }

    void update(int keyDownValue , int delta ) throws SlickException
    {
        if (!transition())
        {

            //Move LEFT and check collition
            if (keyDownValue == Input.KEY_LEFT) {
                //playerX--;
                playerX = playerX - playerSpeed;
                playerPoly.setX(playerX);
                if (entityCollisionWith() || playerX < 0){
                    playerX = playerX+playerSpeed;
                    playerPoly.setX(playerX);
                }
                player = left;

                lastKeyValue = keyDownValue;
            }
            //Move RIGHT and check collition
            if (keyDownValue == Input.KEY_RIGHT ){

                playerX = playerX + playerSpeed;
                playerPoly.setX(playerX);
                if (entityCollisionWith()|| playerX > 1024 - 15){
                    playerX = playerX - playerSpeed;
                    playerPoly.setX(playerX);
                }
                player = right;

                lastKeyValue = keyDownValue;
            }
            //Move UP and check collition
            if (keyDownValue == Input.KEY_UP) {

                // playerY--;
                playerY = playerY - playerSpeed;
                playerPoly.setY(playerY+32);
                if (entityCollisionWith() || playerY < -15){
                    playerY = playerY + playerSpeed;
                    playerPoly.setY(playerY+32);
                }
                player = up;

                lastKeyValue = keyDownValue;
            }
            //Move DOWN and check collition
            if (keyDownValue == Input.KEY_DOWN ) {
                //playerY++;
                playerY = playerY + playerSpeed;
                playerPoly.setY(playerY+32);
                if (entityCollisionWith()|| playerY > 768 - 50){
                    playerY = playerY - playerSpeed;
                    playerPoly.setY(playerY+32);
                }
                player = down;

                lastKeyValue = keyDownValue;
            }
            if (!(keyDownValue == Input.KEY_DOWN) && !(keyDownValue == Input.KEY_UP)  &&
                    !(keyDownValue == Input.KEY_RIGHT) && !(keyDownValue == Input.KEY_LEFT)) {
                if(lastKeyValue == Input.KEY_DOWN){ player = downStill;}
                if(lastKeyValue == Input.KEY_UP)   {player = upStill;}
                if(lastKeyValue == Input.KEY_RIGHT){player = rightStill;}
                if(lastKeyValue == Input.KEY_LEFT)   {player = leftStill;}
            }

            if(null != positionXHistory && positionXHistory.size() >= followingDistance)
            {
                positionX5Ago = positionXHistory.get(positionXHistory.size() - followingDistance);
                positionY5Ago =  positionYHistory.get(positionYHistory.size() - followingDistance);
               //Poly5Ago = positionPolyHistory.get(positionPolyHistory.size() - followingDistance);
            }

            if(keyDownValue == Input.KEY_SPACE)
            {
                //ie.  if im main character (not following anyone)
                if (null == this.following)
                {

                    //Play the sound
                    if( !attackSound.playing() || delta > 30 ) {
                      attackSound.play( 1.0f , 0.50f );
                    }

                    if(lastKeyValue == Input.KEY_DOWN){ player = attackDown;}
                    if(lastKeyValue == Input.KEY_UP)   {player = attackUp;}
                    if(lastKeyValue == Input.KEY_RIGHT){player = attackRight;}
                    if(lastKeyValue == Input.KEY_LEFT)   {player = attackLeft;}
                    if (this.spellAttackRight != null)
                    {
                        ArrayList<NPC> enemies = new ArrayList<NPC>();
                        // ArrayList<NPC> enemies ;
                        if(this.name == "William" || this.name == "Astraea")
                        {
                            if (player == attackRight)
                            {
                                attackAnimation = spellAttackRight;
                                enemies = map.enemySquareMap(this.playerX+32,this.playerY,this.playerX+224,this.playerY+64);
                            }
                            if (player == attackLeft)
                            {
                                attackAnimation = spellAttackLeft;
                                enemies = map.enemySquareMap(this.playerX-192,this.playerY,this.playerX,this.playerY+64);
                            }
                            if (player == attackUp)
                            {
                                attackAnimation = spellAttackRight;
                                enemies = map.enemySquareMap(this.playerX,this.playerY-192,this.playerX+32,this.playerY);
                            }
                            if (player == attackDown)
                            {
                                attackAnimation = spellAttackLeft;
                                enemies = map.enemySquareMap(this.playerX,this.playerY+64,this.playerX+32,this.playerY+256);
                            }

                            for (NPC enemy:enemies)
                            {
                                enemy.onHit(NPC.CENTER,this.getAttackDamage(), NPC.FIRE_DAMAGE); ;

                            }
                        }
                    }
                }
            }

            if(null != following && following.positionX5Ago != -1)
            {
                //if (following.playerX == this.playerX)
                //{
                playerX = following.positionX5Ago;
                //}
                //if(following.playerY == this.playerY)
                //{
                playerY = following.positionY5Ago;
                //}
                playerPoly.setX(playerX);

                playerPoly.setY(playerY+32);

                //System.out.println("PC: "+this.name + " x: "+ this.playerX + " y: "+ this.playerY + " Poly x: " + this.playerPoly.getX() + " Poly y: "+ this.playerPoly.getY());

            }
        }

        //System.out.println(this.name +" x:" +this.playerX + " y:"+this.playerY);

        actionHistory.push(keyDownValue);
        positionXHistory.push(playerX);
        positionYHistory.push(playerY);

        updateHealthBar ();


    }

    int getAttackDamage()
    {
        return 1;
    }

    void  receiveDamage(double x)
    {
        this.health -= x;
        updateHealthBar();
    }


    void  updateHealthBar()
    {
        if (this.health == 60)
        {
            this.healthBar.current = this.healthBar.green;
        }
        else if(this.health >= 50)
        {
            this.healthBar.current = this.healthBar.greenDamaged;
        }
        else if(this.health >= 40)
        {
            this.healthBar.current = this.healthBar.yellow;
        }
        else if(this.health >= 30)
        {
            this.healthBar.current = this.healthBar.yellowDamaged;
        }
        else if(this.health >= 20)
        {
            this.healthBar.current = this.healthBar.red;

        }
        else if(this.health >= 10)
        {
            this.healthBar.current = this.healthBar.redDamaged;
        }
        else //if(this.health <10)
        {
            this.healthBar.current = this.healthBar.empty;
        }
    }
}
