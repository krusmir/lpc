/**
 * Created with IntelliJ IDEA.
 * User: iamgod
 * Date: 5/21/12
 * Time: 3:32 PM
 * To change this template use File | Settings | File Templates.
 */

import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import java.util.ArrayList;

public class BlockMap {

    public static final int BLOCK_SPACES = 9;
    public static final int SKY_3 = 8;
    public static final int SKY_2 = 7;
    public static final int SKY_1 = 6;
    public static final int STRUCTURES_3 = 5;
    public static final int STRUCTURES_2 = 4;
    public static final int STRUCTURES_1 = 3;
    public static final int GROUND_3 = 2;
    public static final int GROUND_2 = 1;
    public static final int GROUND_1 = 0;

    public TiledMap tmap;
    public int mapWidth;
    public int mapHeight;
    private int square[] = {1,1,31,1,31,31,1,31}; //square shaped tile
    public  ArrayList<Object> entities;

    public ArrayList<NPC> NPC;
    public PC mainChar;

    public ArrayList<ArrayList<NPC>> waves;

    public BlockMap(String ref) throws SlickException {

        NPC = new ArrayList<NPC>();
        entities = new ArrayList<Object>();
        tmap = new TiledMap(ref, "data");
        mapWidth = tmap.getWidth() * tmap.getTileWidth();
        mapHeight = tmap.getHeight() * tmap.getTileHeight();

        for (int x = 0; x < tmap.getWidth(); x++) {
            for (int y = 0; y < tmap.getHeight(); y++) {
                int tileID = tmap.getTileId(x, y, BLOCK_SPACES);
                //System.out.println(tileID);
                if (tileID != 0) {
                    entities.add(
                            new Block(x * 32, y * 32, square, "square")
                    );
                }
            }
        }
    }
       public void generateWaves(int strength, int mobQuantity, int numWaves, boolean rising)
       {
           if (waves == null)
           {
               waves = new ArrayList<ArrayList<NPC>>();
           }

           int mobOffset = 0;

           for (int x = 0; x < numWaves;x++)
           {
                this.waves.add(generateWave(strength,mobQuantity));

               if ( ( strength < 5 ) ){
                   strength++;
               }

               if ( x%10 == 0 ){
                   mobOffset++;
               }

               if ( x%7 == 0 ){
                   mobQuantity = 4 + mobOffset;
               }

               if( mobQuantity < 8 ){
                   mobQuantity +=1 ;
               }
               else{
                   mobQuantity = 8;
               }
           }

       }

    public void getNextWave()
    {

        if(this.waves.size() >0)
        {
            this.NPC = this.waves.get(0);
            this.waves.remove(0);
        }
    }

    public void removeDeadNPC()
    {
        if (this.NPC != null)
        {
            ArrayList<NPC> npcToRemove = new ArrayList<NPC>();
            for(NPC mob:this.NPC)
            {
                if (mob.health == 0)
                {

                    npcToRemove.add(mob);
                }
            }
            for (NPC mobToRemove:npcToRemove)
            {
                this.mainChar.score += 1 + mobToRemove.playerSpeed ;
                this.mainChar.score += 2 + mobToRemove.health ;
                this.mainChar.score += 3 + mobToRemove.attackDamage;
                this.NPC.remove(mobToRemove);
            }

        }
    }

    public ArrayList<NPC> generateWave(int strength, int quantity)
    {
        ArrayList<NPC> enemyWave= new ArrayList<NPC>();
        int xpos;
        int ypos;
        int animationSpeed;

        if (strength == 0)   //green blobs + skeletons
        {
            //set north mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000) * -1 -100);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,100,2,0.2F);
                blob.name = "Blob";
                blob.map = this;
                enemyWave.add(blob);
            }
            //set west mobs0
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) * -1 -100);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,100,2,0.4F);
                blob.name = "Blob_west" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set south mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000)  +868);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,100,2,0.2F);
                blob.name = "Blob_south" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set east mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) +1124);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,100,2,0.2F);
                blob.name = "Blob_east" + x;
                blob.map = this;
                enemyWave.add(blob);
            }


        }
        else if(strength == 1)   //green bloobs + red blobs + skeletons
        {
            //set north mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000) * -1 -100);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,125,2,0.4F);
                blob.name = "Blob";
                blob.map = this;
                enemyWave.add(blob);
            }
            //set west mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) * -1 -100);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,125,2,0.4F);
                blob.name = "Blob_west" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set south mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000)  +868);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,125,2,0.4F);
                blob.name = "Blob_south" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set east mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) +1124);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,125,2,0.4F);
                blob.name = "Blob_east" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
        }
        else if(strength == 2)    //red blobs + skeletons +red skeletons
        {
            //set north mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000) * -1 -100);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,150,2,0.8F);
                blob.name = "Blob";
                blob.map = this;
                enemyWave.add(blob);
            }
            //set west mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) * -1 -100);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,150,2,0.8F);
                blob.name = "Blob_west" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set south mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000)  +868);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,150,2,0.8F);
                blob.name = "Blob_south" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set east mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) +1124);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,150,2,0.8F);
                blob.name = "Blob_east" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
        }
        else if(strength == 3)   //zombies + red skeletons +  blue skeletons
        {
            //set north mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000) * -1 -100);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,175,3,1.6F);
                blob.name = "Blob";
                blob.map = this;
                enemyWave.add(blob);
            }
            //set west mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) * -1 -100);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,175,3,1.6F);
                blob.name = "Blob_west" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set south mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000)  +868);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,175,3,1.6F);
                blob.name = "Blob_south" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set east mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) +1124);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,175,3,1.6F);
                blob.name = "Blob_east" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
        }
        else if(strength == 4)  //
        {
            //set north mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000) * -1 -100);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,200,4,3.2F);
                blob.name = "Blob";
                blob.map = this;
                enemyWave.add(blob);
            }
            //set west mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) * -1 -100);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,200,4,3.2F);
                blob.name = "Blob_west" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set south mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000)  +868);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,200,4,3.2F);
                blob.name = "Blob_south" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set east mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) +1124);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,200,4,3.2F);
                blob.name = "Blob_east" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
        }
        else if(strength == 5)
        {
            //set north mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000) * -1 -100);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,250,4,6.4F);
                blob.name = "Blob";
                blob.map = this;
                enemyWave.add(blob);
            }
            //set west mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) * -1 -100);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,250,4,6.4F);
                blob.name = "Blob_west" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set south mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) (Math.random() * 1024);
                ypos =(int) ((Math.random() * 1000)  +868);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,250,4,6.4F);
                blob.name = "Blob_south" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
            //set east mobs
            for(int x = 0; x < quantity;x++)
            {
                xpos =(int) ((Math.random() * 1000) +1124);
                ypos =(int)( Math.random() *768);
                animationSpeed = (int)(Math.random()*100) + 50 ;
                NPC blob = new Blob_mob_aggresive(animationSpeed,xpos,ypos,250,4,6.4F);
                blob.name = "Blob_east" + x;
                blob.map = this;
                enemyWave.add(blob);
            }
        }
        return enemyWave;

    }

    public void setRandomPosition(int direction, NPC mob)
    {

    }
    //public ArrayList<NPC> enemySquareMap(int topX, int topY,int bottomX, int bottomY)
    public ArrayList<NPC> enemySquareMap(float bottomX, float bottomY,float topX, float topY)
    {
       // System.out.println(topX+" "+topY+" "+bottomX +" "+bottomY);
        ArrayList<NPC>  enemyMap = new ArrayList<NPC>();


        for(NPC enemy : NPC)
        {
          /*  System.out.println(enemy.playerX>bottomX);
            System.out.println(enemy.playerY>bottomY);
            System.out.println(enemy.playerX<topX);
            System.out.println(enemy.playerY<topY);
            System.out.println("");*/


            if(enemy.playerX < topX && enemy.playerX> bottomX && enemy.playerY < topY && enemy.playerY > bottomY)
            {
                enemyMap.add(enemy);
            }
        }

        return enemyMap;
    }
}